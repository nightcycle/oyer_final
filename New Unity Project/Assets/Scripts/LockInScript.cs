﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class LockInScript : MonoBehaviour, IPointerClickHandler
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
	
	public void OnPointerClick(PointerEventData ped)
    {
		if (MainUI.current_selected_move != ""){
			MainUI.current_selected_move = "";
			if (MainUI.current_section == "stance"){
				MainUI.current_section = "attack";
			}else if (MainUI.current_section == "attack"){
				MainUI.current_section = "defense";
			}else if (MainUI.current_section == "defense"){
				MainUI.current_section = "stance";
			}
		}	
	}	
}
