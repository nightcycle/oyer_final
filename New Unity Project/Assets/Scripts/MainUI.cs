﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Move{
	public string Name{get;set;}
	public string Style{get;set;}
	public string Description{get;set;}
	public string Category{get;set;}
	public float Multiplier{get;set;}
	public string MultiplierEffect{get;set;}
	public float CostMultiplier{get;set;}
	public string CostEffect{get;set;}
	
	public Move(string name, string style, string cat, float multi, string multi_effect, float cost_multi, string cost_effect)
    {
        this.Name = name;
		this.Style = style;
		this.Category = cat;
		this.Description = name + " is a " + style.ToLower() + " " + cat.ToLower() + " that increases your " + multi_effect.ToLower() + " multiplier by " + (Mathf.Round((multi-1)*100)).ToString() + "% at the cost of " + (Mathf.Round((1-cost_multi)*100)).ToString() + "% less " + cost_effect.ToLower() + ".";
		this.Multiplier = multi;
		this.MultiplierEffect = multi_effect;
		this.CostMultiplier = cost_multi;
		this.CostEffect = cost_effect;
    }
	
}

public class MainUI : MonoBehaviour
{
	//Data
	public Dictionary<string, Move> stances = new Dictionary<string, Move>{
		{"Chudan", new Move("Chudan-no-kamae", "Neutral", "Stance", 1.15f, "Attack", 0.8f, "Defense")},
		{"Jodan", new Move("Jodan-no-kamae", "Dangerous", "Stance", 1.4f, "Attack", 0.6f, "Defense")},	
		{"Hasso", new Move("Hasso-no-kamae", "Safe", "Stance", 1.05f, "Attack", 0.9f, "Defense")},	
		{"Waki", new Move("Waki-gamae", "Neutral", "Stance", 1.15f, "Attack", 0.8f, "Defense")},	
	};

	public Dictionary<string, Move> attacks = new Dictionary<string, Move>{
		{"Nuki", new Move("Nuki waza", "Neutral", "Attack", 1.15f, "Attack", 0.8f, "Defense")},
		{"Suriage", new Move("Suriage waza", "Dangerous", "Attack", 1.4f, "Attack", 0.6f, "Defense")},	
		{"Kaeshi", new Move("Kaeshi waza", "Safe", "Attack", 1.05f, "Attack", 0.9f, "Defense")},	
		{"Uchiotoshi", new Move("Uchiotoshi waza", "Neutral", "Attack", 1.15f, "Attack", 0.8f, "Defense")},	
		{"Renzoku", new Move("Renzoku waza", "Dangerous", "Attack", 1.4f, "Attack", 0.6f, "Defense")},
	};

	public Dictionary<string, Move> defenses = new Dictionary<string, Move>{
		{"Lean", new Move("Lean", "Neutral", "Defense", 1.15f, "Defense", 0.8f, "Attack")},
		{"Deflect", new Move("Deflect", "Dangerous", "Defense", 1.4f, "Defense", 0.6f, "Attack")},	
		{"Back-Up", new Move("Back-Up", "Safe", "Defense", 1.05f, "Defense", 0.9f, "Attack")},	
	};
	
	public static string section_title_text = "STANCE SELECTION";
	
	public int exp = 153;	
	public int max_exp = 800;

	public int your_max_health = 20;
	public int your_health;
	
	public int their_max_health = 20;
	public int their_health;
	
	public int your_intuition_points = 4;
	public int their_intution_points = 4;
	
	//UI elements
	public GameObject moves_frame;
	public GameObject lock_in_choice_button;
	public GameObject section_title_textlabel;
	public GameObject intuition_point_frame;
	public GameObject move_desc_textlabel;
	public GameObject section_desc_textlabel;
	
	public GameObject your_fighter_name_textlabel;
	public GameObject their_fighter_name_textlabel;
	
	public GameObject your_health_bar_frame;
	public GameObject their_health_bar_frame;
	
	public GameObject exp_bar_frame;
	
	public GameObject button_prefab;

	public Sprite dangerous_image;
	public Sprite neutral_image;
	public Sprite safe_image;

	//Misc Variables
	public static string current_section = "stance";	
	public string previous_section;
	public static string current_selected_move;
	public string previous_selected_move;
	
	public string our_stance;
	public string their_stance;
	
	public string our_attack;
	public string their_attack;
	
	public string our_defense;
	public string their_defense;
	
	void update_bar_frame(GameObject bar_frame, int val, int max_val, string bonus_msg){
		Transform fill_trans = bar_frame.transform.Find("Fill Area").Find("Fill");
		GameObject text_obj = bar_frame.transform.Find("Text").gameObject;
		fill_trans.localScale = new Vector3((float)val/(float)max_val, 1, 1);
		text_obj.GetComponent<Text>().text = val + "/" + max_val + " " + bonus_msg;
	}
	
	public void update_UI(){
		section_title_textlabel.GetComponent<Text>().text = current_section.ToUpper() +" SELECTION";		
		section_desc_textlabel.GetComponent<Text>().text = "Choose your " + current_section + ". Once you've decided press the 'Lock In Choice' button!";
		 
		update_bar_frame(your_health_bar_frame, your_health, your_max_health, "HP");
		update_bar_frame(their_health_bar_frame, their_health, their_max_health, "HP");
		update_bar_frame(exp_bar_frame, exp, max_exp, "EXP");
		
		foreach (Transform i_point in intuition_point_frame.transform){
			int actual_name = 5; //because C# is making it difficult to directly convert a string to an int
			if (i_point.gameObject.name == "1"){
				actual_name = 1;
			}else if (i_point.gameObject.name == "2"){
				actual_name = 2;
			}else if (i_point.gameObject.name == "3"){
				actual_name = 3;
			}else if (i_point.gameObject.name == "4"){
				actual_name = 4;
			}
			if (actual_name > your_intuition_points){
				i_point.gameObject.SetActive(false);
			}else{
				i_point.gameObject.SetActive(true);
			}
		}

		void update_buttons(Dictionary<string, Move> current_dict){

			foreach(Transform button in moves_frame.transform){
				Destroy(button.gameObject);	
			}
			int count = 0;
			foreach (KeyValuePair<string, Move> action in current_dict){
				if (current_selected_move == action.Value.Name){
					move_desc_textlabel.GetComponent<Text>().text = action.Value.Description;
				}
				if (count < 5){
					GameObject new_button = Instantiate(button_prefab);
					new_button.transform.Find("Text").GetComponent<Text>().text = action.Key;
					
					if (action.Value.Style == "Safe"){
						new_button.transform.Find("Image").GetComponent<Image>().sprite = safe_image;
					}else if (action.Value.Style == "Neutral"){
						new_button.transform.Find("Image").GetComponent<Image>().sprite = neutral_image;
					}else if (action.Value.Style == "Dangerous"){
						new_button.transform.Find("Image").GetComponent<Image>().sprite = dangerous_image;
					}
					
					new_button.transform.parent = moves_frame.transform;
					new_button.transform.name = action.Value.Name;
					new_button.transform.localPosition = new Vector3(-200+(120*count),7,0);
					count++;
				}
			}
		}
		
		if (current_section == "stance"){
			update_buttons(stances);
		}else if (current_section == "attack"){
			update_buttons(attacks);
		}else if (current_section == "defense"){
			update_buttons(defenses);
		}

	}
	
	void Awake(){
		Screen.SetResolution(789,444,false);
	}
	
	void Start(){
		your_health = your_max_health;
		their_health = their_max_health;
		previous_section = current_section;
		update_UI();
	}

	void Update(){
		if (previous_section != current_section || previous_selected_move != current_selected_move){
			previous_section = current_section;
			previous_selected_move = current_selected_move;
			update_UI();
		}
	}
}
